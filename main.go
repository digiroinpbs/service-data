package main

import (
	"net/http"
	"encoding/json"
	"bytes"
	"github.com/go-redis/redis"
	"github.com/magiconair/properties"
	"github.com/buger/jsonparser"
	"io/ioutil"
)

func initRedis() *redis.Client{
	p := properties.MustLoadFile("/etc/service.conf", properties.UTF8)
	address := p.GetString("redis.name","localhost")+":"+p.GetString("redis.port","6379")
	client := redis.NewClient(&redis.Options{
		Addr:     address,
		Password: p.GetString("redis.password",""),
		DB:       p.GetInt("redis.db",0),
	})
	return client
}

func main() {
	http.HandleFunc("/digiroin/service/data", data)
	http.ListenAndServe(":7056", nil)
}

type Pulsa struct {
	Cashtag string 	`json:"cashtag"`
	Phone string	`json:"phone"`
	Type string		`json:"type"`
	Denom string	`json:"denom"`
}

func data(w http.ResponseWriter, r *http.Request) {
	response := Pulsa{}
	client := &http.Client{}
	redis := initRedis()
	err := json.NewDecoder(r.Body).Decode(&response)
	result :=""
	if(err!=nil){
		w.WriteHeader(http.StatusBadRequest)
		result = `{"Error":"`+err.Error()+`"}`
	}else{
		redisValue := redis.HGet("service","data$"+response.Cashtag).Val()
		if(redisValue==""){
			result = `{"Error":"cashtag not found"}`
			w.WriteHeader(http.StatusBadRequest)
		}else{
			bodyReq := `{"Phone":"`+response.Phone+`","Type":"`+response.Type+`","Denom":"`+response.Denom+`"}`
			var jsonStr = []byte(bodyReq)
			req, err := http.NewRequest("POST", redisValue, bytes.NewBuffer(jsonStr))
			resp, err := client.Do(req)
			if(err==nil){
				res,err := ioutil.ReadAll(resp.Body)
				if err==nil{
					if resp.StatusCode==200{
						error,_ := jsonparser.ParseString(res)
						result = error
					}else{
						error,_ := jsonparser.ParseString(res)
						result =error
						w.WriteHeader(http.StatusInternalServerError)
					}
				}else{
					result = `{"Error":"`+err.Error()+`"}`
					w.WriteHeader(http.StatusInternalServerError)
				}
			}else{
				result = `{"Error":"`+err.Error()+`"}`
				w.WriteHeader(http.StatusInternalServerError)
			}
		}
	}
	w.Write([]byte(result))
}
